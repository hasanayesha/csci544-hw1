import sys
import os


if len(sys.argv) > 3 or len(sys.argv) < 3:
	print ('Usage: python3 assignment1.py    path/to/inputfile    path/to/outputfile')
	sys.exit(1)

else:
	inputfile = sys.argv[1]
	outputfile = sys.argv[2]


try:
	fileWriter = open(outputfile,'w')
except FileNotFoundError:
	print('Output File could not be written into.')
	sys.exit(1)
except IOError:
	print('Could not write output to file.')
	sys.exit(1)

try:
	with open(inputfile) as fp:
		readData = fp.readlines()
except FileNotFoundError:
	print ('Input File could not be found.')
	sys.exit(1)


for line in readData:
	sentence = line.split()
	sentLength = len(sentence)
	fileWriter.write(str(sentLength)+'\n')

fileWriter.close()